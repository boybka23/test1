"""import cv2
import numpy as np
# Đọc ảnh
image = cv2.imread('test2.png',1)
# Xử lý ảnh bằng lọc trung bình
kernel = np.ones((3,3),np.float32)/9
processed_image = cv2.filter2D(image,-1,kernel)
# Hiển thị ảnh 
cv2.imshow('Mean Filter Processing', processed_image)
# Lưu thành ảnh mới
cv2.imwrite('processed_image.png', processed_image)
# Dừng màn hình
cv2.waitKey(0)"""

import cv2
import numpy as np
# Đọc ảnh
image = cv2.imread('(15).png',1)
# Xử lý ảnh bằng lọc trung bình
kernel = np.ones((5,5),np.float32)/25
processed_image = cv2.filter2D(image,-1,kernel)
# Hiển thị ảnh 
cv2.imshow('Mean Filter Processing', processed_image)
# Lưu thành ảnh mới
cv2.imwrite('processed_image.png', processed_image)
# Dừng màn hình
cv2.waitKey(0)

