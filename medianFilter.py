import cv2

# đọc ảnh
image = cv2.imread('(15).png',1)
# Xử lý ảnh bằng lọc trung vị
processed_image = cv2.medianBlur(image,1 )
# Hiển thị ảnh
cv2.imshow('Median Filter Processing', processed_image)
# Lưu thành 1 ảnh khác
cv2.imwrite('processed_image2.png', processed_image)
# Dừng màn hình để xem kết quả
cv2.waitKey(0)
